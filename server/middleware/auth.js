// import jwt from "jsonwebtoken";
// import { asyncMiddleware } from "../utils/asyncWrapper.js";

// const auth = asyncMiddleware(async (req, res, next) => {
//   const token = req.headers.authorization.split(" ")[1];
//   const isCustomAuth = token.length < 500;
//   let decodeData;

//   if (token && isCustomAuth) {
//     decodeData = jwt.verify(
//       token,
//       "my-secret-signature-for-2022-with-js-mastery"
//     );
//     req.userId = decodeData?.id;
//   } else {
//     decodeData = jwt.verify(token);
//     req.userId = decodeData?.sub;
//   }
//   next();
// });
// export default auth;

import jwt from "jsonwebtoken";

const secret = "my-secret-signature-for-2022-with-js-mastery";

const auth = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const isCustomAuth = token.length < 500;

    let decodedData;

    if (token && isCustomAuth) {
      decodedData = jwt.verify(token, secret);

      req.userId = decodedData?.id;
    } else {
      decodedData = jwt.decode(token);

      req.userId = decodedData?.sub;
    }

    next();
  } catch (error) {
    console.log(error);
  }
};

export default auth;
