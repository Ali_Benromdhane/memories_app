import { asyncMiddleware } from "../utils/asyncWrapper.js";
import postModel from "../models/postModel.js";
import mongoose from "mongoose";

export const getPosts = asyncMiddleware(async (req, res) => {
  const post = await postModel.find();
  res.status(200).json(post);
});

export const createPost = asyncMiddleware(async (req, res) => {
  const post = req.body;
  const newPost = new postModel({
    ...post,
    creator: req.userId,
    createdAt: new Date().toISOString(),
  });

  await newPost.save();
  res.status(201).json(newPost);
});

export const updatePost = asyncMiddleware(async (req, res) => {
  const { id } = req.params;
  const { title, message, creator, selectedFile, tags } = req.body;

  if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).send(`No post with id: ${id}`);

  const updatedPost = { creator, title, message, tags, selectedFile, _id: id };

  await postModel.findByIdAndUpdate(id, updatedPost, { new: true });

  res.json(updatedPost);
});

export const deletePost = asyncMiddleware(async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).send("No post with that id");

  await postModel.findByIdAndRemove(id);

  res.json({ message: "Post deleted successfully !!" });
});

export const likePost = asyncMiddleware(async (req, res) => {
  const { id } = req.params;

  if (!req.userId) return res.json({ message: "Unauthenticated" });
  if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).send("No post with that id");

  const post = await postModel.findById(id);

  const index = post.likes.indexOf((id) => id === String(req.userId));

  if (index === -1) {
    post.likes.push(req.userId);
  } else {
    post.likes.filter((id) => id !== String(req.userId));
  }
  const updatedPostLikeCount = await postModel.findByIdAndUpdate(id, post, {
    new: true,
  });

  res.json(updatedPostLikeCount);
});
