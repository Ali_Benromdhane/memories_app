import { asyncMiddleware } from "../utils/asyncWrapper.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import userModel from "../models/userModel.js";

export const signup = asyncMiddleware(async (req, res) => {
  const { email, password, firstName, lastName, passwordConfirm } = req.body;

  const existingUser = await userModel.findOne({ email });
  if (existingUser)
    return res.status(400).json({ message: "User already exists." });

  const hashedPassword = await bcrypt.hash(password, 12);
  const result = await userModel.create({
    email,
    password: hashedPassword,
    name: `${firstName} ${lastName}`,
  });

  const token = jwt.sign(
    { email: result.email, id: result._id },
    "my-secret-signature-for-2022-with-js-mastery",
    {
      expiresIn: "100d",
    }
  );
  res.status(200).json({ result, token });
});

export const signin = asyncMiddleware(async (req, res) => {
  const { email, password } = req.body;

  const existingUser = await userModel.findOne({ email });
  const isPasswordCorrect = await bcrypt.compare(
    password,
    existingUser.password
  );
  if (!existingUser || !isPasswordCorrect) {
    return res
      .status(404)
      .json({ message: "Invalid email or password, please try again" });
  }

  const token = jwt.sign(
    { email: existingUser.email, id: existingUser._id },
    "my-secret-signature-for-2022-with-js-mastery",
    {
      expiresIn: "5h",
    }
  );
  res.status(200).json({ result: existingUser, token });
});
