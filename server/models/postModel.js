import mongoose from "mongoose";
const { Schema, model } = mongoose;

const postSchema = new Schema({
  creator: String,
  name: String,
  title: String,
  message: String,
  tags: [String],
  selectedFile: String,
  likes: {
    type: [String],
    default: [],
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
});

const postModel = model("postModel", postSchema);

export default postModel;
