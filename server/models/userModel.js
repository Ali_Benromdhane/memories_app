import mongoose from "mongoose";
const { Schema, model } = mongoose;

const userSchema = new Schema({
  firstName: String,
  lastName: String,
  name: String,
  email: String,
  password: String,
  passwordConfirm: String,
  id: String,
});

const userModel = model("userModel", userSchema);

export default userModel;
