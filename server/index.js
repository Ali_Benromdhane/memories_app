import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import postRoutes from "./routes/postRoutes.js";
import userRoutes from "./routes/userRoutes.js";

import { globalErrorHandler } from "./controllers/erorrController.js";

const app = express();

app.use(express.json({ limit: "30mb", extended: true })); // To parse the incoming requests with JSON payloads
app.use(express.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());

app.use("/posts", postRoutes);
app.use("/users", userRoutes);

app.use(globalErrorHandler);

const PORT = 5000;

mongoose
  .connect(
    "mongodb+srv://second:second@cluster0.tktlw.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() =>
    app.listen(PORT, () => console.log(`Server running on port:${PORT}`))
  )
  .catch((error) => console.log(error.message));
