import axios from "axios";
// const URL = "https://memories-project2050.herokuapp.com/posts";
const API = axios.create({ baseURL: "http://localhost:5000" });
//Send Token to our backend via request
API.interceptors.request.use((req) => {
  if (localStorage.getItem("profile")) {
    req.headers.Authorization = `Bearer ${
      JSON.parse(localStorage.getItem("profile")).token
    }`;
  }
  return req;
});

export const fetchPosts = () => {
  return API.get("/posts");
};

export const createPost = (newPost) => {
  return API.post("/posts", newPost);
};

export const updatePost = (id, updatedPost) => {
  return API.patch(`${"/posts"}/${id}`, updatedPost);
};

export const deletePost = (id) => {
  return API.delete(`${"/posts"}/${id}`);
};

export const likePost = (id) => {
  return API.patch(`${"/posts"}/${id}/likePost`);
};

export const signIn = (formData) => API.post("/users/signin", formData);
export const signUp = (formData) => API.post("/users/signup", formData);
