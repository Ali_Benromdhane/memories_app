export const ErrorFallback = ({ error }) => {
  const refreshpPage = () => {
    window.location.reload();
  };
  return (
    <div role="alert">
      <p>Something went wrong:</p>
      <pre>{error.message}</pre>
      <button onClick={refreshpPage}>Try again</button>
    </div>
  );
};
