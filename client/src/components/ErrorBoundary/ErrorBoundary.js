import { ErrorBoundary } from "react-error-boundary";
import { ErrorFallback } from "./ErrorFallback";
import { errorHandler } from "./errorHandler";

const ErrorBoundaryCom = ({ children }) => {
  return (
    <ErrorBoundary FallbackComponent={ErrorFallback} onError={errorHandler}>
      {children}
    </ErrorBoundary>
  );
};
export default ErrorBoundaryCom;
