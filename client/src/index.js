import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reducers from "./store/reducers";
import App from "./App";
import ErrorBoundaryCom from "./components/ErrorBoundary/ErrorBoundary";
import React from "react";

const store = createStore(reducers, compose(applyMiddleware(thunk)));

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundaryCom>
      <App />
    </ErrorBoundaryCom>
  </Provider>,

  document.getElementById("root")
);
